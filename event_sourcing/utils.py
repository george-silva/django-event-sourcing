import functools


def method_dispatch(func):
    dispatcher = functools.singledispatch(func)

    def wrapper(*args, **kw):
        return dispatcher.dispatch(args[1].__class__)(*args, **kw)

    wrapper.register = dispatcher.register
    # copies the `func` docstrings and other definitions to `wrapper`
    functools.update_wrapper(wrapper, func)
    return wrapper
