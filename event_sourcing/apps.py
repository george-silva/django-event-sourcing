# -*- coding: utf-8
from django.apps import AppConfig


class EventSourcingConfig(AppConfig):
    name = "event_sourcing"
