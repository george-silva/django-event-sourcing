# -*- coding: utf-8 -*-
import uuid
from django.db import models
from django.contrib.postgres.fields import JSONField


class Event(models.Model):
    """Represents a single event in our event streams.
    """

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)

    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    aggregate_uuid = models.UUIDField(db_index=True)

    model = models.CharField(max_length=128, null=True)  # not sure if needed

    name = models.CharField(max_length=128)

    data = JSONField()

    def __str__(self):
        return f"{self.aggregate_uuid} - {self.name}"

    class Meta:

        verbose_name = "Event"
        verbose_name_plural = "Events"


class Version(models.Model):

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)

    aggregate_uuid = models.UUIDField(db_index=True)

    version = models.IntegerField(default=1)

    class Meta:

        verbose_name = "Aggregate"
        verbose_name_plural = "Aggregates"
