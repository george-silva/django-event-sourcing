from datetime import datetime
from .utils import method_dispatch


class Event:
    def __init__(self, **kwargs):
        self.keys = kwargs.items()
        [setattr(self, key, value) for key, value in kwargs.items()]

    def __eq__(self, other):
        return all([getattr(self, key) == getattr(other, key)])

    def persist(self):
        pass


class Aggregate:
    @classmethod
    def create(cls):
        raise NotImplementedError

    def __init__(self, events: list):
        print("inited")
        for event in events:
            self.apply(event)
        self.events = list()

    def snapshot(self) -> dict:
        for event in self.events:
            event.persist()
