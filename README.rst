=============================
Django Event Sourcing
=============================

.. image:: https://badge.fury.io/py/django-event-sourcing.svg
    :target: https://badge.fury.io/py/django-event-sourcing

.. image:: https://travis-ci.org/george-silva/django-event-sourcing.svg?branch=master
    :target: https://travis-ci.org/george-silva/django-event-sourcing

.. image:: https://codecov.io/gh/george-silva/django-event-sourcing/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/george-silva/django-event-sourcing

EventSourcing for Django

Documentation
-------------

The full documentation is at https://django-event-sourcing.readthedocs.io.

Quickstart
----------

Install Django Event Sourcing::

    pip install django-event-sourcing

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'event_sourcing.apps.EventSourcingConfig',
        ...
    )

Add Django Event Sourcing's URL patterns:

.. code-block:: python

    from event_sourcing import urls as event_sourcing_urls


    urlpatterns = [
        ...
        url(r'^', include(event_sourcing_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
