from django.test import TestCase
from event_sourcing.aggregates import Event, Aggregate
from event_sourcing.utils import method_dispatch


class Order(Aggregate):
    class OrderCreated(Event):
        pass

    class OrderStatusChanged(Event):
        pass

    @classmethod
    def create(cls, user_id: int):
        order_created = Order.OrderCreated(user_id=user_id)
        order_created.status = "new"
        instance = cls([order_created])
        return instance

    @method_dispatch
    def apply(self, event: Event):
        raise ValueError("Unsupported event.")

    @apply.register(OrderCreated)
    def _(self, event: OrderCreated):
        self.user_id = event.user_id
        self.status = event.status

    @apply.register(OrderStatusChanged)
    def _(self, event: OrderStatusChanged):
        self.status = event.status


class TestOrder:
    def test_create_order(self):

        order = Order.create(1)
        assert order.user_id == 1
        assert order.status == "new"

    def test_create_and_change_status(self):

        order = Order.create(1)
        status_changed = Order.OrderStatusChanged(status="completed")
        order.apply(status_changed)
        assert order.status == "completed"

    def test_multiple_orders(self):

        order1 = Order.create(1)
        order2 = Order.create(2)
        assert order1.user_id != order2.user_id
        status_changed = Order.OrderStatusChanged(status="completed")
        order2.apply(status_changed)
        assert order1.status != order2.status
        assert order1.status == "new"
        assert order2.status == "completed"
