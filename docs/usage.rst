=====
Usage
=====

To use Django Event Sourcing in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'event_sourcing.apps.EventSourcingConfig',
        ...
    )

Add Django Event Sourcing's URL patterns:

.. code-block:: python

    from event_sourcing import urls as event_sourcing_urls


    urlpatterns = [
        ...
        url(r'^', include(event_sourcing_urls)),
        ...
    ]
