============
Installation
============

At the command line::

    $ easy_install django-event-sourcing

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-event-sourcing
    $ pip install django-event-sourcing
